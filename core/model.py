import os
import torch

from torch.autograd import Variable
from torch import nn
from torch.nn import functional as F
from .layers import ConvNorm, LinearNorm
from .global_style_tokens import StyleNet
from .utils import to_gpu, get_mask_from_lengths, speaker_mode


class LocationLayer(nn.Module):
    def __init__(self, attention_n_filters, attention_kernel_size,
                 attention_dim):
        super(LocationLayer, self).__init__()
        padding = int((attention_kernel_size - 1) / 2)
        self.location_conv = ConvNorm(2, attention_n_filters,
                                      kernel_size=attention_kernel_size,
                                      padding=padding, bias=False, stride=1,
                                      dilation=1)
        self.location_dense = LinearNorm(attention_n_filters, attention_dim,
                                         bias=False, w_init_gain='tanh')

    def forward(self, attention_weights_cat):
        processed_attention = self.location_conv(attention_weights_cat)
        processed_attention = processed_attention.transpose(1, 2)
        processed_attention = self.location_dense(processed_attention)
        return processed_attention


class Attention(nn.Module):
    def __init__(self, attention_rnn_dim, embedding_dim, attention_dim,
                 attention_location_n_filters, attention_location_kernel_size):
        super(Attention, self).__init__()
        self.query_layer = LinearNorm(attention_rnn_dim, attention_dim,
                                      bias=False, w_init_gain='tanh')
        self.memory_layer = LinearNorm(embedding_dim, attention_dim, bias=False,
                                       w_init_gain='tanh')
        self.v = LinearNorm(attention_dim, 1, bias=False)
        self.location_layer = LocationLayer(attention_location_n_filters,
                                            attention_location_kernel_size,
                                            attention_dim)
        self.score_mask_value = -float("inf")

    def get_alignment_energies(self, query, processed_memory,
                               attention_weights_cat):
        """
        PARAMS
        ------
        query: decoder output (batch, n_mel_channels * n_frames_per_step)
        processed_memory: processed encoder outputs (B, T_in, attention_dim)
        attention_weights_cat: cumulative and prev. att weights (B, 2, max_time)

        RETURNS
        -------
        alignment (batch, max_time)
        """

        processed_query = self.query_layer(query.unsqueeze(1))
        processed_attention_weights = self.location_layer(attention_weights_cat)
        energies = self.v(torch.tanh(
            processed_query + processed_attention_weights + processed_memory))

        energies = energies.squeeze(-1)
        return energies

    def forward(self, attention_hidden_state, memory, processed_memory,
                attention_weights_cat, mask):
        """
        PARAMS
        ------
        attention_hidden_state: attention rnn last output
        memory: encoder outputs
        processed_memory: processed encoder outputs
        attention_weights_cat: previous and cummulative attention weights
        mask: binary mask for padded data
        """
        alignment = self.get_alignment_energies(
            attention_hidden_state, processed_memory, attention_weights_cat)

        if mask is not None:
            alignment.data.masked_fill_(mask, self.score_mask_value)

        attention_weights = F.softmax(alignment, dim=1)
        attention_context = torch.bmm(attention_weights.unsqueeze(1), memory)
        attention_context = attention_context.squeeze(1)

        return attention_context, attention_weights


class Prenet(nn.Module):
    def __init__(self, in_dim, sizes):
        super(Prenet, self).__init__()
        in_sizes = [in_dim] + sizes[:-1]
        self.layers = nn.ModuleList(
            [LinearNorm(in_size, out_size, bias=False)
             for (in_size, out_size) in zip(in_sizes, sizes)])

    def forward(self, x):
        for linear in self.layers:
            x = F.dropout(F.relu(linear(x)), p=0.5, training=True)
        return x


class Postnet(nn.Module):
    """Postnet
        - Five 1-d convolution with 512 channels and kernel size 5
    """

    def __init__(self, hparams):
        super(Postnet, self).__init__()
        self.dropout = nn.Dropout(0.5)
        self.convolutions = nn.ModuleList()

        self.convolutions.append(
            nn.Sequential(
                ConvNorm(hparams.n_mel_channels, hparams.postnet_embedding_dim,
                         kernel_size=hparams.postnet_kernel_size, stride=1,
                         padding=int((hparams.postnet_kernel_size - 1) / 2),
                         dilation=1, w_init_gain='tanh'),
                nn.BatchNorm1d(hparams.postnet_embedding_dim))
        )

        for i in range(1, hparams.postnet_n_convolutions - 1):
            self.convolutions.append(
                nn.Sequential(
                    ConvNorm(hparams.postnet_embedding_dim,
                             hparams.postnet_embedding_dim,
                             kernel_size=hparams.postnet_kernel_size, stride=1,
                             padding=int((hparams.postnet_kernel_size - 1) / 2),
                             dilation=1, w_init_gain='tanh'),
                    nn.BatchNorm1d(hparams.postnet_embedding_dim))
            )

        self.convolutions.append(
            nn.Sequential(
                ConvNorm(hparams.postnet_embedding_dim, hparams.n_mel_channels,
                         kernel_size=hparams.postnet_kernel_size, stride=1,
                         padding=int((hparams.postnet_kernel_size - 1) / 2),
                         dilation=1, w_init_gain='linear'),
                nn.BatchNorm1d(hparams.n_mel_channels))
            )

    def forward(self, x):
        for i in range(len(self.convolutions) - 1):
            x = self.dropout(torch.tanh(self.convolutions[i](x)))

        x = self.dropout(self.convolutions[-1](x))

        return x


class Encoder(nn.Module):
    """Encoder module:
        - Three 1-d convolution banks
        - Bidirectional LSTM
    """
    def __init__(self, hparams):
        super(Encoder, self).__init__()
        self.dropout = nn.Dropout(0.5)

        convolutions = []
        for _ in range(hparams.encoder_n_convolutions):
            conv_layer = nn.Sequential(
                ConvNorm(hparams.encoder_embedding_dim,
                         hparams.encoder_embedding_dim,
                         kernel_size=hparams.encoder_kernel_size, stride=1,
                         padding=int((hparams.encoder_kernel_size - 1) / 2),
                         dilation=1, w_init_gain='relu'),
                nn.BatchNorm1d(hparams.encoder_embedding_dim))
            convolutions.append(conv_layer)
        self.convolutions = nn.ModuleList(convolutions)

        self.lstm = nn.LSTM(hparams.encoder_embedding_dim,
                            int(hparams.encoder_embedding_dim / 2), 1,
                            batch_first=True, bidirectional=True)

    def forward(self, x, input_lengths):
        for conv in self.convolutions:
            x = self.dropout(F.relu(conv(x)))

        x = x.transpose(1, 2)

        # pytorch tensor are not reversible, hence the conversion
        input_lengths = input_lengths.cpu().numpy()
        x = nn.utils.rnn.pack_padded_sequence(
            x, input_lengths, batch_first=True)

        self.lstm.flatten_parameters()
        outputs, _ = self.lstm(x)

        outputs, _ = nn.utils.rnn.pad_packed_sequence(
            outputs, batch_first=True)

        return outputs

    def inference(self, x):
        for conv in self.convolutions:
            x = self.dropout(F.relu(conv(x)))

        x = x.transpose(1, 2)

        self.lstm.flatten_parameters()
        outputs, _ = self.lstm(x)

        return outputs


class Decoder(nn.Module):
    def __init__(self, hparams):
        super(Decoder, self).__init__()
        self.n_mel_channels = None
        self.n_frames_per_step = None
        self.encoder_embedding_dim = None
        self.attention_rnn_dim = None
        self.decoder_rnn_dim = None
        self.prenet_dim = None
        self.max_decoder_steps = None
        self.gate_threshold = None
        self.p_attention_dropout = None
        self.p_decoder_dropout = None

        self.set_params(hparams)

        self.prenet = Prenet(
            hparams.n_mel_channels * hparams.n_frames_per_step,
            [hparams.prenet_dim, hparams.prenet_dim])

        self.attention_rnn = nn.LSTMCell(
            hparams.prenet_dim + self.encoder_embedding_dim,
            hparams.attention_rnn_dim)

        self.attention_layer = Attention(
            hparams.attention_rnn_dim, self.encoder_embedding_dim,
            hparams.attention_dim, hparams.attention_location_n_filters,
            hparams.attention_location_kernel_size)

        self.decoder_rnn = nn.LSTMCell(
            hparams.attention_rnn_dim + self.encoder_embedding_dim,
            hparams.decoder_rnn_dim, 1)

        self.linear_projection = LinearNorm(
            hparams.decoder_rnn_dim + self.encoder_embedding_dim,
            hparams.n_mel_channels*hparams.n_frames_per_step)

        self.gate_layer = LinearNorm(
            hparams.decoder_rnn_dim + self.encoder_embedding_dim, 1,
            bias=True, w_init_gain='sigmoid')

    def set_params(self, hparams):
        self.n_mel_channels = hparams.n_mel_channels
        self.n_frames_per_step = hparams.n_frames_per_step
        self.encoder_embedding_dim = hparams.encoder_embedding_dim
        if hparams.speaker_mode != speaker_mode.NONE:
            self.encoder_embedding_dim += hparams.speaker_embedding_dim
        self.attention_rnn_dim = hparams.attention_rnn_dim
        self.decoder_rnn_dim = hparams.decoder_rnn_dim
        self.prenet_dim = hparams.prenet_dim
        self.max_decoder_steps = hparams.max_decoder_steps
        self.gate_threshold = hparams.gate_threshold
        self.p_attention_dropout = hparams.p_attention_dropout
        self.p_decoder_dropout = hparams.p_decoder_dropout

    def get_go_frame(self, memory):
        """ Gets all zeros frames to use as first decoder input
        PARAMS
        ------
        memory: decoder outputs

        RETURNS
        -------
        decoder_input: all zeros frames
        """
        B = memory.size(0)
        decoder_input = Variable(memory.data.new(
            B, self.n_mel_channels * self.n_frames_per_step).zero_())
        return decoder_input

    def initialize_decoder_states(self, memory, mask):
        """ Initializes attention rnn states, decoder rnn states, attention
        weights, attention cumulative weights, attention context, stores memory
        and stores processed memory
        PARAMS
        ------
        memory: Encoder outputs
        mask: Mask for padded data if training, expects None for inference
        """
        B = memory.size(0)
        MAX_TIME = memory.size(1)

        attention_hidden = Variable(memory.data.new(
            B, self.attention_rnn_dim).zero_())
        attention_cell = Variable(memory.data.new(
            B, self.attention_rnn_dim).zero_())

        decoder_hidden = Variable(memory.data.new(
            B, self.decoder_rnn_dim).zero_())
        decoder_cell = Variable(memory.data.new(
            B, self.decoder_rnn_dim).zero_())

        attention_weights = Variable(memory.data.new(
            B, MAX_TIME).zero_())
        attention_weights_cum = Variable(memory.data.new(
            B, MAX_TIME).zero_())
        attention_context = Variable(memory.data.new(
            B, self.encoder_embedding_dim).zero_())

        processed_memory = self.attention_layer.memory_layer(memory)

        return attention_hidden, attention_cell, decoder_hidden, decoder_cell, attention_weights, attention_weights_cum, attention_context, memory, processed_memory, mask

    def parse_decoder_inputs(self, decoder_inputs):
        """ Prepares decoder inputs, i.e. mel outputs
        PARAMS
        ------
        decoder_inputs: inputs used for teacher-forced training, i.e. mel-specs

        RETURNS
        -------
        inputs: processed decoder inputs

        """
        # (B, n_mel_channels, T_out) -> (B, T_out, n_mel_channels)
        decoder_inputs = decoder_inputs.transpose(1, 2)
        decoder_inputs = decoder_inputs.view(
            decoder_inputs.size(0),
            int(decoder_inputs.size(1)/self.n_frames_per_step), -1)
        # (B, T_out, n_mel_channels) -> (T_out, B, n_mel_channels)
        decoder_inputs = decoder_inputs.transpose(0, 1)
        return decoder_inputs

    def parse_decoder_outputs(self, mel_outputs, gate_outputs, alignments):
        """ Prepares decoder outputs for output
        PARAMS
        ------
        mel_outputs:
        gate_outputs: gate output energies
        alignments:

        RETURNS
        -------
        mel_outputs:
        gate_outpust: gate output energies
        alignments:
        """
        # (T_out, B) -> (B, T_out)
        alignments = torch.stack(alignments).transpose(0, 1)
        # (T_out, B) -> (B, T_out)
        gate_outputs = torch.stack(gate_outputs).transpose(0, 1)
        gate_outputs = gate_outputs.contiguous()
        # (T_out, B, n_mel_channels) -> (B, T_out, n_mel_channels)
        mel_outputs = torch.stack(mel_outputs).transpose(0, 1).contiguous()
        # decouple frames per step
        mel_outputs = mel_outputs.view(
            mel_outputs.size(0), -1, self.n_mel_channels)
        # (B, T_out, n_mel_channels) -> (B, n_mel_channels, T_out)
        mel_outputs = mel_outputs.transpose(1, 2)

        return mel_outputs, gate_outputs, alignments

    def decode(self, decoder_input, decoder_states):
        """ Decoder step using stored states, attention and memory
        PARAMS
        ------
        decoder_input: previous mel output

        RETURNS
        -------
        mel_output:
        gate_output: gate output energies
        attention_weights:
        """
        attention_hidden, attention_cell, decoder_hidden, decoder_cell, attention_weights, attention_weights_cum, attention_context, memory, processed_memory, mask = decoder_states

        cell_input = torch.cat((decoder_input, attention_context), -1)
        attention_hidden, attention_cell = self.attention_rnn(
            cell_input, (attention_hidden, attention_cell))
        attention_hidden = F.dropout(
            attention_hidden, self.p_attention_dropout, self.training)

        attention_weights_cat = torch.cat(
            (attention_weights.unsqueeze(1),
             attention_weights_cum.unsqueeze(1)), dim=1)
        attention_context, attention_weights = self.attention_layer(
            attention_hidden, memory, processed_memory,
            attention_weights_cat, mask)

        attention_weights_cum += attention_weights
        decoder_input = torch.cat(
            (attention_hidden, attention_context), -1)
        decoder_hidden, decoder_cell = self.decoder_rnn(
            decoder_input, (decoder_hidden, decoder_cell))
        decoder_hidden = F.dropout(
            decoder_hidden, self.p_decoder_dropout, self.training)

        decoder_hidden_attention_context = torch.cat(
            (decoder_hidden, attention_context), dim=1)
        decoder_output = self.linear_projection(
            decoder_hidden_attention_context)

        gate_prediction = self.gate_layer(decoder_hidden_attention_context)

        decoder_states = attention_hidden, attention_cell, decoder_hidden, decoder_cell, attention_weights, attention_weights_cum, attention_context, memory, processed_memory, mask
        return decoder_output, gate_prediction, attention_weights, decoder_states

    def forward(self, memory, decoder_inputs, memory_lengths):
        """ Decoder forward pass for training
        PARAMS
        ------
        memory: Encoder outputs
        decoder_inputs: Decoder inputs for teacher forcing. i.e. mel-specs
        memory_lengths: Encoder output lengths for attention masking.

        RETURNS
        -------
        mel_outputs: mel outputs from the decoder
        gate_outputs: gate outputs from the decoder
        alignments: sequence of attention weights from the decoder
        """

        decoder_input = self.get_go_frame(memory).unsqueeze(0)
        decoder_inputs = self.parse_decoder_inputs(decoder_inputs)
        decoder_inputs = torch.cat((decoder_input, decoder_inputs), dim=0)
        decoder_inputs = self.prenet(decoder_inputs)

        decoder_states = self.initialize_decoder_states(
            memory, mask=~get_mask_from_lengths(memory_lengths))

        mel_outputs, gate_outputs, alignments = [], [], []

        while len(mel_outputs) < decoder_inputs.size(0) - 1:
            decoder_input = decoder_inputs[len(mel_outputs)]
            mel_output, gate_output, attention_weights, decoder_states = self.decode(
                decoder_input, decoder_states)
            mel_outputs.append(mel_output)
            gate_outputs.append(gate_output.squeeze(1))
            alignments.append(attention_weights)

        mel_outputs, gate_outputs, alignments = self.parse_decoder_outputs(
            mel_outputs, gate_outputs, alignments)

        return mel_outputs, gate_outputs, alignments

    def inference(self, memory):
        """ Decoder inference
        PARAMS
        ------
        memory: Encoder outputs

        RETURNS
        -------
        mel_outputs: mel outputs from the decoder
        gate_outputs: gate outputs from the decoder
        alignments: sequence of attention weights from the decoder
        """
        decoder_input = self.get_go_frame(memory)

        decoder_states = self.initialize_decoder_states(memory, mask=None)

        mel_count = 0
        while True:
            decoder_input = self.prenet(decoder_input)
            mel_output, gate_output, alignment, decoder_states = self.decode(decoder_input, decoder_states)

            yield mel_output
            mel_count += 1
            if torch.sigmoid(gate_output.data) > self.gate_threshold:
                break
            elif mel_count == self.max_decoder_steps:
                print("Warning! Reached max decoder steps")
                break

            decoder_input = mel_output
        yield None


class Tacotron2(nn.Module):
    def __init__(self, hparams):
        super(Tacotron2, self).__init__()
        self.mask_padding = None
        self.n_mel_channels = None
        self.speaker_mode = None
        self.lang = None

        self.embedding = None
        self.mask_embedding = None
        self.speaker_embedding = None

        self.set_params(hparams)

        torch.nn.init.xavier_uniform_(self.embedding.weight.data)
        if hparams.speaker_mode == speaker_mode.ENTITY_EMBEDDING and hasattr(
                hparams, "pretrained_speaker_embedding"
        ) and hparams.pretrained_speaker_embedding is not None and os.path.exists(
            hparams.pretrained_speaker_embedding
        ):
            print('use pretrained_speaker_embedding')
            speaker_dict = {
                speaker: idx for idx, speaker in enumerate(hparams.speaker)
            }

            pretrained_speaker_embedding = torch.load(
                hparams.pretrained_speaker_embedding)
            for speaker, data in pretrained_speaker_embedding.items():
                if speaker in speaker_dict:
                    self.speaker_embedding.weight.data[speaker_dict[speaker]] = data
        else:
            torch.nn.init.xavier_uniform_(self.speaker_embedding.weight.data)

        self.encoder = Encoder(hparams)
        self.stylenet = StyleNet(hparams) if hparams.use_global_style_tokens else None
        self.decoder = Decoder(hparams)
        self.postnet = Postnet(hparams)

    def set_params(self, hparams):
        self.mask_padding = hparams.mask_padding
        self.n_mel_channels = hparams.n_mel_channels
        self.speaker_mode = hparams.speaker_mode
        self.lang = hparams.lang

        self.embedding = nn.Embedding(
            hparams.n_symbols, hparams.symbols_embedding_dim)

        if hparams.lang in ('eng2', 'cmu'):
            self.mask_embedding = nn.Embedding(2, hparams.symbols_embedding_dim)
            torch.nn.init.xavier_uniform_(self.mask_embedding.weight.data)
        else:
            self.mask_embedding = None

        if hparams.speaker_mode == speaker_mode.ENTITY_EMBEDDING:
            self.speaker_embedding = nn.Embedding(len(hparams.speaker),
                                                  hparams.speaker_embedding_dim)
        else:
            self.speaker_embedding = None

    def parse_batch(self, batch):
        text_padded, input_lengths, mel_padded, gate_padded, \
            output_lengths, speakers = batch
        text_padded = to_gpu(text_padded).long()
        max_len = torch.max(input_lengths.data).item()
        input_lengths = to_gpu(input_lengths).long()
        mel_padded = to_gpu(mel_padded).float()
        gate_padded = to_gpu(gate_padded).float()
        output_lengths = to_gpu(output_lengths).long()
        if self.speaker_mode == speaker_mode.ENTITY_EMBEDDING:
            speakers = to_gpu(speakers).long()
        elif self.speaker_mode == speaker_mode.SPEAKER_ENCODING:
            speakers = to_gpu(speakers).float()
        else:
            speakers = None

        return (
            (text_padded, input_lengths, mel_padded, max_len, output_lengths, speakers),
            (mel_padded, gate_padded))

    def parse_output(self, outputs, output_lengths=None):
        if self.mask_padding and output_lengths is not None:
            mask = ~get_mask_from_lengths(output_lengths)
            mask = mask.expand(self.n_mel_channels, mask.size(0), mask.size(1))
            mask = mask.permute(1, 0, 2)

            outputs[0].data.masked_fill_(mask, 0.0)
            outputs[1].data.masked_fill_(mask, 0.0)
            outputs[2].data.masked_fill_(mask[:, 0, :], 1e3)  # gate energies

        return outputs

    def forward(self, inputs):
        inputs, input_lengths, targets, max_len, output_lengths, speakers = inputs
        input_lengths, output_lengths = input_lengths.data, output_lengths.data

        embedded_inputs = self.embedding(inputs).transpose(1, 2)
        if self.mask_embedding is not None:
            if self.lang == 'eng2':
                mask = (65 <= inputs) * (inputs <= 148)
            elif self.lang == 'cmu':
                mask = (65 <= inputs) * (inputs <= 103)
            else:
                raise RuntimeError('Wrong type of lang')
            mask = mask.long()
            embedded_mask = self.mask_embedding(mask).transpose(1, 2)
            embedded_inputs = embedded_inputs + embedded_mask

        encoder_outputs = self.encoder(embedded_inputs, input_lengths)

        if self.speaker_mode != speaker_mode.NONE:
            if self.speaker_embedding:
                speaker_emb = self.speaker_embedding(speakers).unsqueeze(1)
            elif self.speaker_mode == speaker_mode.SPEAKER_ENCODING:
                speaker_emb = speakers.unsqueeze(1)
            speaker_emb = speaker_emb.expand(
                encoder_outputs.size(0),
                encoder_outputs.size(1),
                speaker_emb.size(-1)
            )
            encoder_outputs = torch.cat((encoder_outputs, speaker_emb), dim=2)

        if self.stylenet:
            style_emb = self.stylenet(targets, output_lengths)
            encoder_outputs = encoder_outputs + style_emb

        mel_outputs, gate_outputs, alignments = self.decoder(
            encoder_outputs, targets, memory_lengths=input_lengths)

        mel_outputs_postnet = self.postnet(mel_outputs)
        mel_outputs_postnet = mel_outputs + mel_outputs_postnet

        # DataParallel expects equal sized inputs/outputs, hence padding
        if input_lengths is not None:
            alignments = alignments.unsqueeze(0)
            alignments = nn.functional.pad(
                alignments,
                (0, max_len - alignments.size(3), 0, 0),
                "constant", 0)
            alignments = alignments.squeeze(0)
        return self.parse_output(
            [mel_outputs, mel_outputs_postnet, gate_outputs, alignments],
            output_lengths)

    def inference(self, inputs, style_emb=None, speaker_emb=None, speaker_idx=0, chunk_size=1200):
        embedded_inputs = self.embedding(inputs).transpose(1, 2)
        if self.mask_embedding is not None:
            if self.lang == 'eng2':
                mask = (65 <= inputs) * (inputs <= 148)
            elif self.lang == 'cmu':
                mask = (65 <= inputs) * (inputs <= 103)
            else:
                raise RuntimeError('Wrong type of lang')
            mask = mask.long()
            embedded_mask = self.mask_embedding(mask).transpose(1, 2)
            embedded_inputs = embedded_inputs + embedded_mask

        encoder_outputs = self.encoder.inference(embedded_inputs)

        if self.speaker_mode != speaker_mode.NONE:
            if self.speaker_embedding:
                speakers = torch.tensor([[speaker_idx]]).cuda()
                speaker_emb = self.speaker_embedding(speakers)
            elif self.speaker_mode == speaker_mode.SPEAKER_ENCODING:
                speaker_emb = speaker_emb.unsqueeze(0).unsqueeze(0)
            speaker_emb = speaker_emb.expand(
                encoder_outputs.size(0),
                encoder_outputs.size(1),
                speaker_emb.size(-1)
            )
            encoder_outputs = torch.cat((encoder_outputs, speaker_emb), dim=2)

        if style_emb is not None:
            encoder_outputs = encoder_outputs + style_emb

        mel_output_list = []
        for mel_output in self.decoder.inference(encoder_outputs):
            if mel_output is not None:
                mel_output_list.append(mel_output)
            if chunk_size == len(mel_output_list) or (mel_output is None and 0 < len(mel_output_list)):
                mel_outputs = torch.stack(mel_output_list, dim=1)
                mel_outputs = mel_outputs.view(
                    mel_outputs.size(0), -1, self.n_mel_channels)
                mel_outputs = mel_outputs.transpose(1, 2)

                mel_outputs_postnet = self.postnet(mel_outputs)
                mel_outputs_postnet = mel_outputs + mel_outputs_postnet
                yield mel_outputs_postnet
                mel_output_list.clear()
