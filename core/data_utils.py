import random
import re
import os
import torch
import torch.utils.data

from collections import Counter

from . import layers
from .utils import load_wav_to_torch, load_filepaths_and_text, speaker_mode
from .text import text_to_sequence
from .text.cmudict import CMUDict


class TextMelLoader(torch.utils.data.Dataset):
    """
        1) loads audio,text pairs
        2) normalizes text and converts them to sequences of one-hot vectors
        3) computes mel-spectrograms from audio files.
    """
    def __init__(self, audiopaths_and_text, hparams, shuffle=True):
        self.res_path = hparams.resource_root

        self.audiopaths_and_text = load_filepaths_and_text(
            os.path.join(self.res_path, audiopaths_and_text),
            hparams.sort_by_length) if audiopaths_and_text is not None else []
        self.text_cleaners = hparams.text_cleaners
        self.max_wav_value = hparams.max_wav_value
        self.sampling_rate = hparams.sampling_rate
        self.load_mel_from_disk = hparams.load_mel_from_disk
        self.speaker_mode = hparams.speaker_mode
        self.speaker_dict = {
            speaker: idx for idx, speaker in enumerate(hparams.speaker)
        } if hparams.speaker_mode == speaker_mode.ENTITY_EMBEDDING else None
        self.stft = layers.TacotronSTFT(
            hparams.filter_length, hparams.hop_length, hparams.win_length,
            hparams.n_mel_channels, hparams.sampling_rate, hparams.mel_fmin,
            hparams.mel_fmax)
        random.seed(1234)
        self.data_mapping = None
        if shuffle:
            if self.speaker_dict:
                speaker_counter = Counter((
                    audiopath_and_text[2]
                    for audiopath_and_text in self.audiopaths_and_text
                ))
                weights = [
                    1.0 / speaker_counter[audiopath_and_text[2]]
                    for audiopath_and_text in self.audiopaths_and_text
                ]
                self.mapping_weights = torch.DoubleTensor(weights)
                self.data_mapping = torch.multinomial(
                    self.mapping_weights, len(self), replacement=True).tolist()
            else:
                random.shuffle(self.audiopaths_and_text)
        if hparams.lang in ('eng2'):
            cmu_res_path = os.path.join(self.res_path, 'cmudict-0.7b_fix.txt')
            self.cmu_dict = CMUDict(cmu_res_path)
            self.cmu_pattern = re.compile(r'^(?P<word>[^!\'(),-.:~?]+)(?P<punc>[!\'(),-.:~?]+)$')
        else:
            self.cmu_dict = None
            self.cmu_pattern = None

    def remove_pt(self):
        for audiopath_and_text in self.audiopaths_and_text:
            audiopath = audiopath_and_text[0]
            pt_file = os.path.join(
                self.res_path,
                '{}.pt'.format(audiopath)
            )
            if os.path.exists(pt_file):
                os.remove(pt_file)

    def shuffle(self):
        if self.speaker_dict:
            self.data_mapping = torch.multinomial(
                self.mapping_weights, len(self), replacement=True).tolist()
        else:
            random.shuffle(self.audiopaths_and_text)

    def get_mel_text_pair(self, audiopath_and_text):
        # separate filename and text
        if self.speaker_dict:
            audiopath, text, speaker = audiopath_and_text[0], audiopath_and_text[1], audiopath_and_text[2]
            speaker = self.speaker_dict[speaker]
        else:
            audiopath, text = audiopath_and_text[0], audiopath_and_text[1]
            if self.speaker_mode == speaker_mode.SPEAKER_ENCODING:
                pt_file = os.path.join(
                    self.res_path,
                    '{}.speaker.pt'.format(os.path.splitext(audiopath)[0])
                )
                speaker = torch.load(pt_file, map_location='cpu')
            else:
                speaker = None
        text = self.get_text(text)
        mel = self.get_mel(audiopath)
        return text, mel, speaker

    def get_mel(self, filename):
        pt_file = os.path.join(
            self.res_path,
            '{}.pt'.format(filename)
        )
        if not os.path.exists(pt_file):
            audio = load_wav_to_torch(os.path.join(self.res_path, filename),
                                      self.sampling_rate)
            audio_norm = audio / self.max_wav_value
            audio_norm = audio_norm.unsqueeze(0)
            audio_norm = torch.autograd.Variable(audio_norm, requires_grad=False)
            melspec = self.stft.mel_spectrogram(audio_norm)
            melspec = torch.squeeze(melspec, 0)
            torch.save(melspec, pt_file)
        else:
            melspec = torch.load(pt_file, map_location='cpu')
            assert melspec.size(0) == self.stft.n_mel_channels, (
                'Mel dimension mismatch: given {}, expected {}'.format(
                    melspec.size(0), self.stft.n_mel_channels))

        return melspec

    def get_arpabet(self, word):
        arpabet = self.cmu_dict.lookup(word)
        if arpabet is None:
            match = self.cmu_pattern.search(word)
            if match is None:
                return word
            sub_word = match.group('word')
            arpabet = self.cmu_dict.lookup(sub_word)
            if arpabet is None:
                return word
            punc = match.group('punc')
            arpabet = '{%s}%s' % (arpabet[0], punc)
        else:
            arpabet = '{%s}' % arpabet[0]
        if random.random() < 0.5:
            return word
        else:
            return arpabet

    def get_text(self, text):
        if self.cmu_dict is not None and random.random() < 0.5:
            text = ' '.join([self.get_arpabet(word) for word in text.split(' ')])
        text_norm = torch.IntTensor(text_to_sequence(text, self.text_cleaners))
        return text_norm

    def __getitem__(self, index):
        if self.data_mapping is not None:
            index = self.data_mapping[index]
        return self.get_mel_text_pair(self.audiopaths_and_text[index])

    def __len__(self):
        return len(self.audiopaths_and_text)


class TextMelCollate():
    """ Zero-pads model inputs and targets based on number of frames per setep
    """
    def __init__(self, n_frames_per_step, speaker_mode):
        self.n_frames_per_step = n_frames_per_step
        self.speaker_mode = speaker_mode

    def __call__(self, batch):
        """Collate's training batch from normalized text and mel-spectrogram
        PARAMS
        ------
        batch: [text_normalized, mel_normalized]
        """
        # Right zero-pad all one-hot text sequences to max input length
        input_lengths, ids_sorted_decreasing = torch.sort(
            torch.LongTensor([len(x[0]) for x in batch]),
            dim=0, descending=True)
        max_input_len = input_lengths[0]

        text_padded = torch.LongTensor(len(batch), max_input_len)
        text_padded.zero_()
        for i in range(len(ids_sorted_decreasing)):
            text = batch[ids_sorted_decreasing[i]][0]
            text_padded[i, :text.size(0)] = text

        # Right zero-pad mel-spec with extra single zero vector to mark the end
        num_mels = batch[0][1].size(0)
        max_target_len = max([x[1].size(1) for x in batch])
        if max_target_len % self.n_frames_per_step != 0:
            max_target_len += self.n_frames_per_step - max_target_len % self.n_frames_per_step
            assert max_target_len % self.n_frames_per_step == 0

        # include mel padded and gate padded
        mel_padded = torch.FloatTensor(len(batch), num_mels, max_target_len)
        mel_padded.zero_()
        gate_padded = torch.FloatTensor(len(batch), max_target_len)
        gate_padded.zero_()
        output_lengths = torch.LongTensor(len(batch))
        for i in range(len(ids_sorted_decreasing)):
            mel = batch[ids_sorted_decreasing[i]][1]
            mel_padded[i, :, :mel.size(1)] = mel
            gate_padded[i, mel.size(1)-1:] = 1
            output_lengths[i] = mel.size(1)

        if self.speaker_mode == speaker_mode.ENTITY_EMBEDDING:
            speakers = torch.LongTensor([
                batch[ids_sorted_decreasing[i]][2]
                for i in range(len(ids_sorted_decreasing))
            ])
        elif self.speaker_mode == speaker_mode.SPEAKER_ENCODING:
            speakers = torch.stack([
                batch[ids_sorted_decreasing[i]][2]
                for i in range(len(ids_sorted_decreasing))
            ])
        else:
            speakers = None

        return text_padded, input_lengths, mel_padded, gate_padded, \
            output_lengths, speakers
