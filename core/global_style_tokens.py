import torch

from torch import nn
from torch.nn import functional as F

from .layers import LinearNorm, Conv2dNorm
from .utils import speaker_mode


class ReferenceEncoder(nn.Module):
    def __init__(self, n_mel_channels=80, out_dim=128,
                 filters=(32, 32, 64, 64, 128, 128), kernel_size=(3, 3),
                 stride=(2, 2)):
        super(ReferenceEncoder, self).__init__()

        convolutions = []
        n_convolutions = len(filters)
        filters = [1] + list(filters)
        for conv_idx in range(n_convolutions):
            conv = Conv2dNorm(filters[conv_idx], filters[conv_idx + 1],
                kernel_size=kernel_size, stride=stride, w_init_gain='relu'
            )
            n_mel_channels = conv.calculate_output_size(n_mel_channels, 0)

            conv_layer = nn.Sequential(
                conv,
                nn.BatchNorm2d(filters[conv_idx + 1])
            )
            convolutions.append(conv_layer)
        self.convolutions = nn.ModuleList(convolutions)

        self.gru = nn.GRU(n_mel_channels*filters[-1], out_dim, 1,
                          batch_first=True, bidirectional=False)

    def forward(self, x, input_lengths):
        x = x.unsqueeze(1)
        for conv in self.convolutions:
            input_lengths = conv[0].calculate_output_size(input_lengths, 1)
            x = F.relu(conv(x))

        x = x.view(x.size(0) , -1, x.size(-1))
        x = x.transpose(1, 2)

        input_lengths, indices = torch.sort(input_lengths, descending=True)
        x = torch.index_select(x, dim=0,index=indices)

        # pytorch tensor are not reversible, hence the conversion
        input_lengths = input_lengths.cpu().numpy()
        x = nn.utils.rnn.pack_padded_sequence(
            x, input_lengths, batch_first=True)

        self.gru.flatten_parameters()
        _, outputs = self.gru(x)

        ref_emb = torch.index_select(outputs[0], dim=0,index=torch.sort(indices)[1])
        return ref_emb

    def inference(self, x):
        x = x.unsqueeze(1)
        for conv in self.convolutions:
            x = F.relu(conv(x))

        x = x.view(x.size(0) , -1, x.size(-1))
        x = x.transpose(1, 2)

        self.gru.flatten_parameters()
        _, ref_emb = self.gru(x)
        return ref_emb


class MultiHeadAttention(nn.Module):
    def __init__(self, query_dim, key_dim, out_dim, n_heads):
        super(MultiHeadAttention, self).__init__()

        assert (out_dim % n_heads == 0)
        self.attention_dim = out_dim // n_heads
        self.query_layer = LinearNorm(query_dim, out_dim,
                                      bias=False, w_init_gain='linear')
        self.key_layer = LinearNorm(key_dim, out_dim,
                                    bias=False, w_init_gain='linear')
        self.value_layer = LinearNorm(key_dim, out_dim,
                                      bias=False, w_init_gain='linear')
        self.output_layer = LinearNorm(out_dim, out_dim,
                                       bias=False, w_init_gain='linear')

    def forward(self, in_query, in_key):
        query = self.query_layer(in_query)
        key = self.key_layer(in_key)

        query = query.view(query.size(0), query.size(1), -1, self.attention_dim)
        query = query.permute(0, 2, 1, 3)
        key = key.view(key.size(0), key.size(1), -1, self.attention_dim)
        key = key.permute(0, 2, 3, 1)

        attention_weights = torch.matmul(query, key)
        attention_weights = attention_weights / (self.attention_dim ** 0.5)
        attention_weights = F.softmax(attention_weights, dim=3)
        return self.inference(attention_weights, in_key)

    def inference(self, attention_weights, in_key):
        value = self.value_layer(in_key)
        value = value.view(value.size(0), value.size(1), -1, self.attention_dim)
        value = value.permute(0, 2, 1, 3)

        attention_context = torch.matmul(attention_weights, value)
        attention_context = attention_context.transpose(1, 2).contiguous()
        attention_context = attention_context.view(attention_context.size(0),
                                                   attention_context.size(1),
                                                   -1)

        output = self.output_layer(attention_context)
        return output


class StyleTokenLayer(nn.Module):
    def __init__(self, in_dim, out_dim, n_token, n_heads):
        super(StyleTokenLayer, self).__init__()
        assert (out_dim % n_heads == 0)
        token_dim = out_dim // n_heads

        self.style_tokens = nn.Parameter(torch.FloatTensor(n_token, token_dim))
        self.attention = MultiHeadAttention(in_dim, token_dim, out_dim, n_heads)

        torch.nn.init.xavier_uniform_(
            self.style_tokens, gain=torch.nn.init.calculate_gain('tanh'))

    def forward(self, input):
        query = input.unsqueeze(1)
        keys = F.tanh(self.style_tokens)
        keys = keys.unsqueeze(0).expand(input.size(0), -1, -1)

        style_emb = self.attention(query, keys)
        return style_emb

    def inference(self, config):
        keys = F.tanh(self.style_tokens)
        keys = keys.unsqueeze(0)
        return self.attention.inference(config, keys)


class StyleNet(nn.Module):
    def __init__(self, hparams):
        super(StyleNet, self).__init__()
        self.reference_encoder = ReferenceEncoder(
            hparams.n_mel_channels, hparams.reference_out_dim,
            filters=hparams.reference_filters,
            kernel_size=hparams.reference_kernel_size,
            stride=hparams.reference_stride
        )

        encoder_embedding_dim = hparams.encoder_embedding_dim
        if hparams.speaker_mode != speaker_mode.NONE:
            encoder_embedding_dim += hparams.speaker_embedding_dim

        self.style_token_layer = StyleTokenLayer(
            hparams.reference_out_dim, encoder_embedding_dim,
            n_token=hparams.style_n_tokens, n_heads=hparams.style_n_heads
        )

    def forward(self, mels, mel_lengths):
        ref_emb = self.reference_encoder(mels, mel_lengths)
        style_emb = self.style_token_layer(ref_emb)

        return style_emb

    def inference_by_mel(self, mel):
        mel = mel.unsqueeze(0)
        ref_emb = self.reference_encoder.inference(mel)
        style_emb = self.style_token_layer(ref_emb)

        return style_emb

    def inference_by_config(self, config):
        config = config.unsqueeze(0).unsqueeze(2)
        style_emb = self.style_token_layer.inference(config)
        return style_emb
