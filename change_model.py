import argparse

from client import AcousticClient

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-r', '--remote',
                        nargs='?',
                        dest='remote',
                        help='grpc: ip:port',
                        type=str,
                        default='127.0.0.1:30001')
    parser.add_argument('-m', '--model',
                        nargs='?',
                        dest='model',
                        help='Model Path.',
                        type=str,
                        default="")
    parser.add_argument('-t', '--threshold',
                        nargs='?',
                        dest='threshold',
                        help='gate threshold in range [0, 1]',
                        type=float,
                        default=-1)
    parser.add_argument('-s', '--max_decoder_steps',
                        nargs='?',
                        dest='max_decoder_steps',
                        help='voice length limit: (max decoder steps * hop_length / sampling_rate) second',
                        type=int,
                        default=-1)

    args = parser.parse_args()

    client = AcousticClient(args.remote)
    model_status = client.get_model()

    print('before model_status: {}'.format(model_status))
    if args.model == "":
        model = model_status.model.path
    else:
        model = args.model

    if args.threshold == -1:
        threshold = model_status.model.config['threshold']
    else:
        threshold = args.threshold

    if args.max_decoder_steps == -1:
        max_decoder_steps = model_status.model.config['max_decoder_steps']
    else:
        max_decoder_steps = args.max_decoder_steps

    client.set_model(
        model, threshold, max_decoder_steps
    )

    model_status = client.get_model()
    print('after model_status: {}'.format(model_status))
