import os
import torch
import argparse

from torch.utils.data import DataLoader

from run.runner import load_checkpoint, make_model
from core.data_utils import TextMelLoader, TextMelCollate


def main(file_list, model_filename, device):
    torch.cuda.set_device(device)

    hparams, state_dict = load_checkpoint(model_filename)
    model = make_model(hparams, state_dict)

    dataset = TextMelLoader(file_list, hparams, False)
    collate_fn = TextMelCollate(hparams.n_frames_per_step,
                                hparams.speaker_mode)

    with torch.no_grad():
        data_loader = DataLoader(dataset, sampler=None, num_workers=1,
                                shuffle=False, batch_size=1,
                                pin_memory=False, collate_fn=collate_fn)

        batch_parser = model.parse_batch

        idx = 0
        for batch in data_loader:
            x, y = batch_parser(batch)
            _, mel_outputs_postnet, _, _ = model(x)

            for b_idx in range(mel_outputs_postnet.size(0)):
                audio_path = dataset.audiopaths_and_text[idx][0]
                output_length = x[4][b_idx]
                mel = mel_outputs_postnet[b_idx, :, :output_length]
                pt_path = os.path.join(
                    dataset.res_path,
                    '{}.pt'.format(os.path.splitext(audio_path)[0])
                )
                torch.save(mel, pt_path)
                idx += 1


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-f', "--filelist_path", required=True)
    parser.add_argument('-c', "--checkpoint_path", required=True)
    parser.add_argument('-d', '--device', type=int, default=0)

    args = parser.parse_args()
    main(args.filelist_path, args.checkpoint_path, args.device)
