import os
import yaml
import torch
import grpc
import numpy as np
import logging
import time
import threading

from core.model import Tacotron2
from core.text import text_to_sequence, set_lang
from core.utils import log_error

import tts_pb2

from tts_pb2_grpc import AcousticServicer


logger = logging.getLogger('tacotron')


def load_checkpoint(checkpoint_path):
    checkpoint = torch.load(checkpoint_path, map_location='cpu')
    hparams = checkpoint['hparams']
    hparams.distributed_run = False
    hparams.mask_padding = False
    if not hasattr(hparams, "use_eos"):
        hparams.add_hparam("use_eos", False)
    set_lang(hparams.lang, hparams.text_cleaners, hparams.use_eos)
    return hparams, checkpoint['state_dict']


def make_model(hparams, state_dict):
    model = Tacotron2(hparams).cuda()
    try:
        model = model.module
    except:
        pass
    model.load_state_dict({
        k.replace('module.', ''): v for k, v in state_dict.items()
    })
    model.eval()
    return model


class Tacotron(AcousticServicer):
    def __init__(self, checkpoint_path, device, threshold, max_decoder_steps, mel_chunk_size, is_fp16, config_path):
        self.lock = threading.Lock()
        self.is_fp16 = is_fp16
        self.running_set = set()
        self.waiting_queue = []
        self.config_path = config_path
        self.mel_chunk_size = mel_chunk_size
        if config_path != "":
            if os.path.exists(config_path):
                logger.info('Load Config File: {}'.format(config_path))
                with open(config_path, 'r', encoding='utf-8') as f:
                    config = yaml.load(f, Loader=yaml.FullLoader)
                    checkpoint_path = config['path']
                    threshold = config['threshold']
                    max_decoder_steps = config['max_decoder_steps']
            else:
                self._save_config(checkpoint_path, threshold, max_decoder_steps)
        self.model_status = tts_pb2.ModelStatus()

        torch.cuda.set_device(device)
        self.device = device
        try:
            self._load_checkpoint(checkpoint_path, threshold, max_decoder_steps)
        except Exception as e:
            self.model_status.state = tts_pb2.MODEL_STATE_NONE
            self.model_status.model.Clear()
            logger.exception(e)

    @log_error
    def Txt2Mel(self, in_text, context):
        torch.cuda.set_device(self.device)
        try:
            with torch.no_grad():
                mel_generator = self._inference(in_text, context, self.max_decoder_steps)
                if mel_generator is None:
                    return tts_pb2.MelSpectrogram()

                mel_outputs_postnet = [mel for mel in mel_generator][0]
                sample_length = mel_outputs_postnet.size(-1) * self.mel_config.hop_length
                mel_outputs_postnet = mel_outputs_postnet.reshape(-1).cpu().tolist()

                mel = tts_pb2.MelSpectrogram(data=mel_outputs_postnet,
                                             sample_length=sample_length)
                return mel
        finally:
            with self.lock:
                self.running_set.discard(threading.get_ident())

    @log_error
    def StreamTxt2Mel(self, in_text, context):
        torch.cuda.set_device(self.device)
        try:
            with torch.no_grad():
                mel_generator = self._inference(in_text, context, self.mel_chunk_size)
                if mel_generator is not None:
                    for mel_outputs_postnet in mel_generator:
                        mel_outputs_postnet = mel_outputs_postnet.reshape(-1).cpu().tolist()

                        mel = tts_pb2.MelSpectrogram(data=mel_outputs_postnet)
                        yield mel
        finally:
            with self.lock:
                self.running_set.discard(threading.get_ident())

    @log_error
    def GetMelConfig(self, empty, context):
        if self.model_status.state == tts_pb2.MODEL_STATE_RUNNING:
            return self.mel_config
        else:
            context.set_code(grpc.StatusCode.UNAVAILABLE)
            context.set_details(tts_pb2.ModelState.Name(self.model_status.state))
            return tts_pb2.MelConfig()

    @log_error
    def SetModel(self, in_model, context, is_running=False):
        result = None
        waiting_key = None
        with self.lock:
            if 0 < len(self.waiting_queue):
                waiting_key = threading.get_ident()
                self.waiting_queue.append(waiting_key)
            else:
                if in_model == self.model_status.model or in_model.path == "":
                    if is_running:
                        self.running_set.update([threading.get_ident()])
                    result = tts_pb2.SetModelResult(result=True)
                elif in_model != self.model_status.model:
                    if 0 < len(self.running_set):
                        waiting_key = threading.get_ident()
                        self.waiting_queue.append(waiting_key)
                    else:
                        if is_running:
                            self.running_set.update([threading.get_ident()])
                        result = self._set_model(in_model)

        if waiting_key is not None:
            while True:
                with self.lock:
                    if self.waiting_queue[0] == waiting_key:
                        if in_model == self.model_status.model or in_model.path == "":
                            if is_running:
                                self.running_set.update([threading.get_ident()])
                            del self.waiting_queue[0]
                            result = tts_pb2.SetModelResult(result=True)
                            break
                        elif in_model != self.model_status.model:
                            if 0 == len(self.running_set):
                                if is_running:
                                    self.running_set.update([threading.get_ident()])
                                del self.waiting_queue[0]
                                result = self._set_model(in_model)
                                break
                time.sleep(0.01)
        return result

    @log_error
    def GetModel(self, empty, context):
        return self.model_status

    def _inference(self, in_text, context, chunk_size):
        logger.debug('_inference/in_text/%s', in_text)
        result = self.SetModel(in_text.model, context, True)
        if not result.result:
            context.set_code(grpc.StatusCode.INVALID_ARGUMENT)
            context.set_details(result.error)
            return None
        if self.model_status.state != tts_pb2.MODEL_STATE_RUNNING:
            context.set_code(grpc.StatusCode.UNAVAILABLE)
            context.set_details(tts_pb2.ModelState.Name(self.model_status.state))
            return None
        if self.max_speaker < in_text.speaker:
            context.set_code(grpc.StatusCode.INVALID_ARGUMENT)
            context.set_details("exceeded maxspeaker")
            return None

        text = in_text.text
        sequence = np.array(
            text_to_sequence(text, self.text_cleaners))[None, :]
        if sequence.shape[1] == 0 or (self.use_eos and sequence.shape[1] == 2):
            return None
        sequence = torch.autograd.Variable(
            torch.from_numpy(sequence)).cuda().long()

        return self.model.inference(sequence, None, None, in_text.speaker, chunk_size)

    def _set_model(self, in_model):
        logger.debug('_set_model/in_model/%s', in_model)
        try:
            if not os.path.exists(in_model.path):
                raise RuntimeError("Model {} does not exist.".format(in_model.path))
            threshold = float(in_model.config["threshold"]) if "threshold" in in_model.config else 0.6
            max_decoder_steps = int(in_model.config["max_decoder_steps"]) if "max_decoder_steps" in in_model.config else 1200
        except Exception as e:
            logger.exception(e)
            return tts_pb2.SetModelResult(result=False, error=str(e))

        try:
            self._load_checkpoint(in_model.path, threshold, max_decoder_steps)
            self._save_config(in_model.path, threshold, max_decoder_steps)
            return tts_pb2.SetModelResult(result=True)
        except Exception as e:
            logger.exception(e)
            self.model_status.state = tts_pb2.MODEL_STATE_NONE
            self.model_status.model.Clear()
            return tts_pb2.SetModelResult(result=False, error=str(e))

    def _load_checkpoint(self, checkpoint_path, threshold, max_decoder_steps):
        self.model_status.state = tts_pb2.MODEL_STATE_LOADING

        if checkpoint_path != self.model_status.model.path:
            hparams, state_dict = load_checkpoint(checkpoint_path)
            hparams.gate_threshold = threshold
            hparams.max_decoder_steps = max_decoder_steps
            hparams.fp16_run = False

            has_model = True
            try:
                if hasattr(self, 'model'):
                    self.model.set_params(hparams)
                    self.model.decoder.set_params(hparams)
                    for emb in [self.model.embedding, self.model.mask_embedding, self.model.speaker_embedding]:
                        if emb is not None:
                            emb.cuda()
                            emb.eval()
                            if self.is_fp16:
                                emb.weight.data = emb.weight.data.half()

                    self.model.load_state_dict({
                        k.replace('module.', ''): v for k, v in state_dict.items()
                    })
                else:
                    has_model = False
            except Exception as e:
                logger.exception(e)
                has_model = False

            if not has_model:
                self.model = make_model(hparams, state_dict)
                if self.is_fp16:
                    from apex import amp
                    self.model, _ = amp.initialize(self.model, [], opt_level="O3")

            self.text_cleaners = hparams.text_cleaners
            self.max_decoder_steps = max_decoder_steps
            self.use_eos = hparams.use_eos

            if hparams.mel_fmax is None:
                hparams.mel_fmax = hparams.sampling_rate / 2
            self.mel_config = tts_pb2.MelConfig(
                filter_length=hparams.filter_length,
                hop_length=hparams.hop_length,
                win_length=hparams.win_length,
                n_mel_channels=hparams.n_mel_channels,
                sampling_rate=hparams.sampling_rate,
                mel_fmin=hparams.mel_fmin,
                mel_fmax=hparams.mel_fmax
            )
            self.max_speaker = len(hparams.speaker) - 1

            del hparams
            del state_dict
            torch.cuda.empty_cache()

            self.model_status.model.path = checkpoint_path
        else:
            self.max_decoder_steps = max_decoder_steps
            self.model.decoder.gate_threshold = threshold
            self.model.decoder.max_decoder_steps = max_decoder_steps
        self.model_status.model.config["threshold"] = str(threshold)
        self.model_status.model.config["max_decoder_steps"] = str(max_decoder_steps)

        self.model_status.state = tts_pb2.MODEL_STATE_RUNNING

    def _save_config(self, checkpoint_path, threshold, max_decoder_steps):
        if self.config_path != "":
            with open(self.config_path, 'w', encoding='utf-8') as wf:
                config = {
                    'path': checkpoint_path,
                    'threshold': threshold,
                    'max_decoder_steps': max_decoder_steps
                }
                yaml.dump(config, wf, default_flow_style=False)
                logger.debug('Save Config File: {}'.format(self.config_path))
