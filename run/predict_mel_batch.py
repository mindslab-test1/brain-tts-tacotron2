import os
import sys
import subprocess
import argparse
import torch
import shutil

from uuid import uuid4


def predict_mel_batch(file_list, model_filename, n_distributed, multi_gpu):
    n_gpu = torch.cuda.device_count() if multi_gpu else 1

    with open(file_list, 'r', encoding='utf-8') as rf:
        data_list = rf.readlines()
    sub_data_len = len(data_list) // (n_distributed * n_gpu)
    if len(data_list) % (n_distributed * n_gpu) > 0:
        sub_data_len += 1

    while True:
        tmp_directory_path = 'tmp_{}'.format(uuid4())
        if not os.path.isdir(tmp_directory_path):
            os.makedirs(tmp_directory_path)
            break

    procs = []
    data_idx = 0
    for gpu_idx in range(n_gpu):
        for distributed_idx in range(n_distributed):
            sub_data_list = data_list[data_idx:data_idx+sub_data_len]
            data_idx += sub_data_len
            tmp_file_path = '{}/{}_{}.txt'.format(tmp_directory_path, gpu_idx, distributed_idx)
            with open(tmp_file_path, 'w', encoding='utf-8') as wf:
                wf.writelines(sub_data_list)
            proc = subprocess.Popen([
                str(sys.executable), 'predict_mel.py',
                '-f', str(tmp_file_path),
                '-c', str(model_filename),
                '-d', str(gpu_idx)
            ])
            procs.append(proc)

    for proc in procs:
        proc.wait()

    shutil.rmtree(tmp_directory_path)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-f', "--filelist_path", required=True)
    parser.add_argument('-c', "--checkpoint_path", required=True)
    parser.add_argument('-n', '--n_distributed', type=int, default=1)
    parser.add_argument('-m', '--multi_gpu', action='store_true')

    args = parser.parse_args()

    predict_mel_batch(args.filelist_path, args.checkpoint_path,
                      args.n_distributed, args.multi_gpu)
