import tensorflow as tf

from core.text import set_lang
from core.utils import speaker_mode


def create_hparams(hparams_string=None, verbose=False):
    """Create model hyperparameters. Parse nondefault from given string."""

    hparams = tf.contrib.training.HParams(
        ################################
        # Experiment Parameters        #
        ################################
        epochs=2500,
        iters_per_checkpoint=500,
        seed=1234,
        dynamic_loss_scaling=True,
        fp16_run=False,
        distributed_run=False,
        dist_backend="nccl",
        dist_url="tcp://localhost:54321",
        cudnn_enabled=True,
        cudnn_benchmark=False,

        ################################
        # Data Parameters             #
        ################################
        load_mel_from_disk=False,
        resource_root="resource",
        training_files='multi_filelist/kor_ms_2_audio_text_train_filelist.txt',#'filelists/ljs_audio_text_train_filelist.txt',#
        validation_files='multi_filelist/kor_ms_2_audio_text_val_filelist.txt',#'filelists/ljs_audio_text_val_filelist.txt',#
        lang='kor',#'eng2',#'cht',#
        text_cleaners=['korean_cleaners'],#['english_cleaners'],#['chinese_cleaners'],#
        sort_by_length=False,
        use_eos=True,

        ################################
        # Audio Parameters             #
        ################################
        max_wav_value=32768.0,
        sampling_rate=22050,
        filter_length=1024,
        hop_length=256,
        win_length=1024,
        n_mel_channels=80,
        mel_fmin=0.0,
        mel_fmax=8000.0,

        ################################
        # Model Parameters             #
        ################################
        symbols=None,
        n_symbols=None,
        symbols_embedding_dim=512,

        # Encoder parameters
        encoder_kernel_size=5,
        encoder_n_convolutions=3,
        encoder_embedding_dim=512,

        # Speaker embedding
        speaker_mode=speaker_mode.ENTITY_EMBEDDING,
        speaker=['synthmindslab', 'pororo', 'crowdworks', 'center', 'KVA_W', 'KVA_M', 'care', 'counselor', 'lec01', 'fa', 'ma', 'fv01', 'fv02', 'fv03', 'fv04', 'fv05', 'fv06', 'fv07', 'fv08', 'fv09', 'fv10', 'fv11', 'fv12', 'fv13', 'fv14', 'fv15', 'fv16', 'fv17', 'fv18', 'fv19', 'fv20', 'fx01', 'fx02', 'fx03', 'fx04', 'fx05', 'fx06', 'fx07', 'fx08', 'fx09', 'fx10', 'fx11', 'fx12', 'fx13', 'fx14', 'fx15', 'fx16', 'fx17', 'fx18', 'fx19', 'fx20', 'mv01', 'mv02', 'mv03', 'mv04', 'mv05', 'mv06', 'mv07', 'mv08', 'mv09', 'mv10', 'mv11', 'mv12', 'mv13', 'mv16', 'mv17', 'mv19', 'mv20', 'mw01', 'mw02', 'mw03', 'mw04', 'mw05', 'mw06', 'mw07', 'mw08', 'mw09', 'mw10', 'mw11', 'mw13', 'mw14', 'mw15', 'mw16', 'mw17', 'mw18', 'mw19', 'mw20', ],
        speaker_embedding_dim=256,
        pretrained_speaker_embedding=None,

        # StyleNet parameters
        use_global_style_tokens=False,
        reference_out_dim=256,
        reference_filters=[64, 64, 128, 128, 256, 256],
        reference_kernel_size=[3, 3],
        reference_stride=[2, 2],

        style_n_tokens=10,
        style_n_heads=4,

        # Decoder parameters
        n_frames_per_step=1,  # currently only 1 is supported
        decoder_rnn_dim=1024,
        prenet_dim=256,
        max_decoder_steps=1000,
        gate_threshold=0.5,
        p_attention_dropout=0.1,
        p_decoder_dropout=0.1,

        # Attention parameters
        attention_rnn_dim=1024,
        attention_dim=128,

        # Location Layer parameters
        attention_location_n_filters=32,
        attention_location_kernel_size=31,

        # Mel-post processing network parameters
        postnet_embedding_dim=512,
        postnet_kernel_size=5,
        postnet_n_convolutions=5,

        ################################
        # Optimization Hyperparameters #
        ################################
        use_saved_learning_rate=False,
        learning_rate=1e-3,
        learning_rate_deacy_rate=0.01,
        start_deacy=50000,
        max_deacy_step=50000,
        weight_decay=1e-6,
        grad_clip_thresh=1.0,
        batch_size=64,
        mask_padding=False  # set model's padded outputs to padded values
    )

    if hparams_string:
        tf.logging.info('Parsing command line hparams: %s', hparams_string)
        hparams.parse(hparams_string)

    if verbose:
        tf.logging.info('Final parsed hparams: %s', hparams.values())

    hparams.symbols = set_lang(hparams.lang, hparams.text_cleaners, hparams.use_eos)
    hparams.n_symbols = len(hparams.symbols)

    return hparams
